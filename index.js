//adding express use in our app
var express = require ('express');
var bodyParser= require ('body-parser');
var session = require ('express-session');
var validator = require ('express-validator');
const expressSanitizer = require('express-sanitizer');

//we define the port we are using
const app = express();
const port = 8000

//use of a mysql database
const mysql = require('mysql');
const db = mysql.createConnection ({
    host: 'localhost',
    user: 'root',
    password: '******',
    database: 'recipes'
});

// connect to database
db.connect((err) => {
    if (err) {
        throw err;
    }
    console.log('Connected to database');
});
global.db = db;

//use of express Sanitizer package
app.use(expressSanitizer());

///added for session management
app.use(session({
    secret: 'somerandomstuffs',
    resave: false,
    saveUninitialized: false,
    cookie: {
        expires: 600000
    }
}));

app.use(bodyParser.urlencoded({ extended: true }));
require ('./routes/main')(app);
app.set('views',__dirname + '/views');
app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile);
app.listen(port, () => console.log('Example app listening on port ${port}!'));

