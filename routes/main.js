//exporting our main app function
module.exports = function(app)
{
//inserting express validatio for use
const { check, validationResult } = require('express-validator');

//use of sessions for login and logged in users
const redirectLogin = (req, res, next) => {
if (!req.session.userId ) {
res.redirect('./login')
} else { next (); }
}

//api for editing recipes
app.get('/apirecipe', redirectLogin, function(req,res){
res.render('apirecipe.html')
});

app.post('/apirecipe', function(req,res){
let recipe_name = req.body.recipe_name;
//mysql command to find(in database) the recipe we are inserting in the form
let sqlquery = "SELECT * FROM recipesInfo WHERE recipe_name='" + recipe_name +"'"; // query database to get all the books
// execute sql query
db.query(sqlquery, (err, result) => {
if (err) {
res.redirect('./');
}
res.render('list.ejs', {recipes: result});
});
});

//index.html
app.get('/',function(req,res){
res.render('index.html')
});

//search.html only for logged in users
app.get('/search', redirectLogin, function(req,res){
res.render("search.html");
});

//get results of search
app.get('/search-result', function (req, res) {
res.send('This is the results: ' + req.query.keyword);
});

//register page
app.get('/register', function (req,res) {
res.render('register.html');                                                                     
});

//get details from register forms, and save them to database by creating new informanion for each row, validate and sanitize information inserted
app.post('/registered',[check('username').notEmpty(), check('email').isEmail(), check('password').isLength({ min: 4 })],  function (req,res) {
const errors = validationResult(req);
const plainPassword = req.sanitize(req.body.password);
if (!errors.isEmpty()) {
res.redirect('./register');}
else {
const bcrypt = require('bcrypt');
const saltRounds = 10;
const plainPassword = req.body.password;
//hashing password
bcrypt.hash(plainPassword, saltRounds, function(err, hashedPassword){
//Store hashed password in your database
let sqlquery = "INSERT INTO usersInfo (first, last, email, username, password, hashedPassword) VALUES (?,?,?,?,?,?)";
// execute sql query
let newrecord = [req.body.first, req.body.last, req.body.email, req.body.username, req.body.password, hashedPassword];
db.query(sqlquery, newrecord, (err, result) => {
if (err) {
return console.error(err.message);
}
else
res.send('Congratulations you have been registered and have full access to our edit recipe tools!' + ' Your hashed password is: '+ hashedPassword);
});
});
}
});

//login page
app.get('/login', function (req,res) {
res.render('login.html');
});

//compare passwords when trying to login with the data saved in database when registered
app.post('/loggedin', function (req, res) {
const bcrypt = require('bcrypt');
const plainPassword = req.body.password;
const hashedPassword = "SELECT hashedPassword FROM usersInfo";
//comparing function
bcrypt.compare(plainPassword, hashedPassword, function(err, result) {
if (err) {
return console.error(err.message);}
else
req.session.userId = req.body.username;
res.send('You have successfully logged in! Return to Home page: ' + '<br />'+'<a href='+'./'+'>Home</a>');
});
});

//logout a user
app.get('/logout', redirectLogin, (req,res) => {
req.session.destroy(err => {
if (err) {
return res.redirect('./')
}
res.send('you are now logged out. <a href='+'./'+'>Home</a>');
});
});

//list page where we see recipes added to database
app.get('/list', redirectLogin, function(req, res) {
//access recipes in database
let sqlquery = "SELECT * FROM recipesInfo"; 
// execute sql query
db.query(sqlquery, (err, result) => {
if (err) {
res.redirect('./'); 
}
res.render('list.ejs', {recipes: result});
});
});

//delete recipe page
app.get('/delete', redirectLogin, function (req,res){
res.render('delete.html');
});

//function to delete recipe with name inserted by the user in the form box
app.delete("/deleterecipe/",(req,res) => {
console.log("Keyword is " + req.body.recipe_name);
const rec_name = req.body.recipe_name;
//delete command from database according to recipe name keyword
const queryString = "DELETE FROM recipeInfo WHERE recipe_name = ?";
  connection.query(queryString, [recipe_name], (err, results, fields) => {
    if (err) {
      console.log("Could not delete" + err);
      res.sendStatus(500);
      return;
    }
    console.log("Deleted recipe with name rec_name");
    res.end();
  });
});

//update page
app.get('/update', redirectLogin, function(req,res){
res.render('update.html');
});

//add recipe page
app.get('/addrecipe', redirectLogin, function (req,res) {
res.render('addrecipe.html');
});

//save recipe in database
app.post('/added', function (req,res) {
// saving data in database
let sqlquery = "INSERT INTO recipesInfo (recipe_name, description, username) VALUES (?,?,?)";
// execute sql query
let newrecord = [req.body.recipe_name, req.body.description, req.body.username];
db.query(sqlquery, newrecord, (err, result) => {
if (err) {
return console.error(err.message);
}
else
res.send('Your recipe has been submitted to our records and users will be able to see it from now on: ' + req.body.recipe_name + ' by user: ' + req.body.username);
});
});

     
}


