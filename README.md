# "Book shop" - A Dynamic web application

(Part of my 2nd year of my CS degree. Project for 'Data and the Web' Module.)

Users are able to search for books, publishers, authors containing specific words. Click on a book and see all data relevant to that book as well as checking stock availability of a book. Update stock availability when new books arrive at store or when books are sold to customers.
At least two types of users for the app, customers to search for books reading data related to books from database and admin users to create, update and delete data as well as reading data from database. 



While building this web application we covered briefly all of the topics below:

1) Creating Web Servers in Node.js and Express
2) Building Web Application in Express 
3) Creating Databases (relational, NoSQL) and Database Querying 
4) Authentication and Authorisation in Dynamic Web Applications
5) Sanitisation and validation in Dynamic Web Application
6) APIs


Vim
Node.js
Express
MySQL
Git
